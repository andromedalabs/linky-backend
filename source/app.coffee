hapi = require 'hapi'
mongoose = require 'mongoose'
_ = require 'underscore'
pretty = require 'prettyjson'

server = new hapi.Server()

mongoose.connect 'mongodb://localhost/test'


UserSchema = new mongoose.Schema({
  name: { type: String }
} )

User = mongoose.model 'User', UserSchema

server.connection
    port : 3000

server.route
    method : 'GET'
    path : '/api/users/{username}'
    handler: (request, reply) ->
        user = new User name : request.params.username
        user.save()
        reply 'Hello, ' + encodeURIComponent(request.params.username) + '!'

server.route
    method : 'GET'
    path : '/api/users'
    handler: (request, reply) ->
        User.find {} , (err, documents) ->
            reply documents


server.start ->
    console.log 'Server running at ' + server.info.uri
